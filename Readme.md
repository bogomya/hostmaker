## Hostmakers

Test assignment for Hostmakers interview


### Demo
http://46.101.4.88/

### Requirements

You need to have installed:

* NodeJS 8+
* MySQL 5.7+

### Frontend stack
* TypeScript / React / Redux / Thunk
* Sass, CSS Modules, Webpack
* Jest, Enzyme, TSLint

##### Features:
* Mobile first responsive design
* Typings generation for css modules
* Google map integration (two-way sync with property list and markers on the map)
* Dynamic markers loading on map drag/zoom
* CSS transitions for smooth animation and high FPS
* Chunk-splitting + HMR with webpack 4
* Tests with Jest and Enzyme

### Backend stack
* TypeScript / NodeJS / Express
* MySQL / TypeORM
* TSLint

##### Features:
* API fully written in Typescript
* Using MySQL geo types and functions for working with points
* Migrations setup for version controlling of db schema
* Logging with Winston


### Frontend setup

Go to ```./frontend``` folder

Run ```npm install```

Copy ```.env.sample``` to ```.env``` and set up Google Map api key
(if you don't have api key, check the instruction [how to get key](https://developers.google.com/maps/documentation/javascript/get-api-key) or ask me)

Run ```npm run dev```


### Backend setup

Go to ```./backend``` folder

Run ```npm install```

Copy ```.env.sample``` to ```.env``` and set-up database configuration

Run ```npm run migration:run``` to create database schema

Run ```npm run fixture:load``` to load fixtures

Run ```npm run dev``` to start development mode or ```npm run prod``` to run production build



##### Enjoy


###### What can be improved
* Dockerize the app to simplify development setup and deployment
* Add markers clustering on the map
* Add pagination to display all results
* Use Google map api instead of Haversine formula to precise detection if a property is central or not
* Add screenshot and E2E tests
