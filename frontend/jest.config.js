const path = require("path");

module.exports = {
  testEnvironment: "jsdom",
  testRegex: "src/.*\\.spec\\.tsx?$",
  setupFiles: [
    path.resolve(__dirname, "test", "testSetup.ts"),
  ],
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
      "\\.(scss)$": "<rootDir>/test/styleMock.js",
      "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$": "<rootDir>/test/fileTransformer.js",
  },
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "json",
  ],
  moduleNameMapper: {
      '^hostmaker/(.*)$': '<rootDir>/$1',
  }
};
