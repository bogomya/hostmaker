import * as React from "react";
import * as ReactDOM from "react-dom";
import thunk from "redux-thunk";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";

import { rootReducer } from "hostmaker/src/ducks/state";
import { PropertyDetailsPage } from "hostmaker/src/layout/propertyDetailsPage/PropertyDetailsPage";

const store = createStore(rootReducer, applyMiddleware(thunk));

ReactDOM.render(
    <Provider store={store}>
        <PropertyDetailsPage />
    </Provider>,
    document.getElementById("root"),
);
