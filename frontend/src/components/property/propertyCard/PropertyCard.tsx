import React from "react";
import classnames from "classnames";
import { Property } from "hostmaker/src/ducks/property/types";

import * as cn from "./PropertyCard.scss";

export type PropertyCardProps = {
    property: Property;
    isSelected: boolean;
    isCentral: boolean;
    toggleSelection: (property?: Property) => void;
};

export class PropertyCard extends React.PureComponent<PropertyCardProps> {
    public handleMouseEnter = () => {
        this.props.toggleSelection(this.props.property);
    }

    public handleMouseLeave = () => {
        this.props.toggleSelection();
    }

    public render() {
        const { property, isSelected, isCentral} = this.props;
        return (
            <div
                onMouseEnter={this.handleMouseEnter}
                onMouseLeave={this.handleMouseLeave}
                className={classnames(cn.card, {[cn.selected]: isSelected, [cn.central]: isCentral })}
            >
                <div className={cn.owner}>{property.owner}</div>
                <div className={cn.address}>
                    {Object.values(property.address)
                        .filter((line) => line)
                        .map((line, index) => <div key={index}>{line}</div>)
                    }
                </div>
                <div className={cn.income}>${property.incomeGenerated}</div>
            </div>
        );
    }
}
