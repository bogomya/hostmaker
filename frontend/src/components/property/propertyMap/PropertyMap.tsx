import * as React from "react";
import GoogleMapReact, { ChangeEventValue } from "google-map-react";

import { City, Coordinate, Property } from "hostmaker/src/ducks/property/types";
import {
    PropertyMarker,
    PropertyMarkerProps,
} from "hostmaker/src/components/property/propertyMap/marker/PropertyMarker";
import { isPropertyCentral } from "hostmaker/src/ducks/selectors";

export type PropertyMapStateProps = {
    properties: Property[];
    selectedProperty: Property | undefined;
    city: City;
};

export type PropertyMapDispatchProps = {
    findPropertiesByCoordinates: (northEast: Coordinate, southWest: Coordinate) => void
    selectProperty: (property?: Property) => void;
};

export type PropertyMapProps = PropertyMapStateProps & PropertyMapDispatchProps;

export class PropertyMap extends React.PureComponent<PropertyMapProps> {
    public selectProperty = (hoverKey: string, marker: PropertyMarkerProps) => {
        this.props.selectProperty(marker.property);
    }

    public clearSelection = () => {
        this.props.selectProperty();
    }

    public handleGoogleApiLoaded = (google: {map: any, maps: any}) => {
        return new google.maps.Circle({
            strokeOpacity: 0.5,
            strokeWeight: 2,
            fillOpacity: 0.3,
            map: google.map,
            center: this.props.city.coordinate,
            radius: this.props.city.radius,
        });
    }

    public handleMapChange = debounce((event: ChangeEventValue) => {
        const bounds = event.bounds;
        this.props.findPropertiesByCoordinates(bounds.ne, bounds.sw);
    }, 500);

    public render() {
        const { properties, selectedProperty, city} = this.props;
        return (
            <GoogleMapReact
                onChildMouseEnter={this.selectProperty}
                onChildMouseLeave={this.clearSelection}
                options={{gestureHandling: "greedy"}}
                bootstrapURLKeys={{
                    key: process.env.GOOGLE_MAP_KEY || "",
                }}
                center={city.coordinate}
                yesIWantToUseGoogleMapApiInternals={true}
                onChange={this.handleMapChange}
                onGoogleApiLoaded={this.handleGoogleApiLoaded}
                zoom={10}
            >
                {properties.map((property) => (
                    <PropertyMarker
                        key={property.id}
                        property={property}
                        isSelected={property === selectedProperty}
                        isCentral={isPropertyCentral(property, city)}
                        lat={property.coordinate.lat}
                        lng={property.coordinate.lng}
                    />
                ))}
            </GoogleMapReact>
        );
    }
}

function debounce(func: (...args: any[]) => void, time: number) {
    let timer: ReturnType<typeof setTimeout>;
    return (...args: any[]) => {
        clearTimeout(timer);
        timer = setTimeout(() => func(...args), time);
    };
}
