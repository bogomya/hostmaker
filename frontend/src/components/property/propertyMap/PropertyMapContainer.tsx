import { connect } from "react-redux";
import { HostmakerState } from "hostmaker/src/ducks/state";
import { selectActiveProperty, selectCity, selectProperties } from "hostmaker/src/ducks/selectors";
import { findPropertiesByCoordinates, selectProperty } from "hostmaker/src/ducks/property/property";
import {
    PropertyMap,
    PropertyMapDispatchProps,
    PropertyMapStateProps,
} from "hostmaker/src/components/property/propertyMap/PropertyMap";

const mapStateToProps = (state: HostmakerState): PropertyMapStateProps  => ({
    properties: selectProperties(state),
    selectedProperty: selectActiveProperty(state),
    city: selectCity(state),
});

const dispatchProps: PropertyMapDispatchProps = {
    findPropertiesByCoordinates,
    selectProperty,
};

export const PropertyMapContainer = connect<
    PropertyMapStateProps,
    PropertyMapDispatchProps,
    {},
    HostmakerState
>(mapStateToProps, dispatchProps)(PropertyMap);
