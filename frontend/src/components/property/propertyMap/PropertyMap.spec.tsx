import * as React from "react";
import { shallow } from "enzyme";

const GoogleMapReact = () => <div/>;
jest.mock('google-map-react', () => ({
    __esModule: true,
    namedExport: jest.fn(),
    default: GoogleMapReact,
}));

import { PropertyMap, PropertyMapProps } from "hostmaker/src/components/property/propertyMap/PropertyMap";
import { PropertyMarker } from "hostmaker/src/components/property/propertyMap/marker/PropertyMarker";
import { mockProperty, mockCity } from "hostmaker/src/ducks/property/mock";

const props: PropertyMapProps = {
    properties: [
        mockProperty(),
        mockProperty(),
        mockProperty(),
    ],
    selectedProperty: undefined,
    city: mockCity(),
    findPropertiesByCoordinates: jest.fn(),
    selectProperty: jest.fn(),
};

describe("PropertyMap", () => {
    it("should render markers for all passed properties", () => {
        const wrapper = shallow(<PropertyMap {...props}/>);
        expect(wrapper.find(PropertyMarker)).toHaveLength(props.properties.length);
    });

    it("should pass city center coordinate as map center", () => {
        const wrapper = shallow(<PropertyMap {...props}/>);
        expect(wrapper.find(GoogleMapReact).props()).toHaveProperty("center", props.city.coordinate);
    });

    it("should mark selected property marker", () => {
        const wrapper = shallow(<PropertyMap {...props} selectedProperty={props.properties[1]}/>);
        const markers = wrapper.find(PropertyMarker);
        expect(markers.get(0).props).toHaveProperty("isSelected", false);
        expect(markers.get(1).props).toHaveProperty("isSelected", true);
    });
});

