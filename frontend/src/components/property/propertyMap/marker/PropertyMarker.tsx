import * as React from "react";
import classnames from "classnames";
import { ChildComponentProps } from "google-map-react";

import { Property } from "hostmaker/src/ducks/property/types";
import * as cn from "hostmaker/src/components/property/propertyMap/marker/PropertyMarker.scss";

export interface PropertyMarkerProps extends ChildComponentProps {
    property: Property;
    isSelected: boolean;
    isCentral: boolean;
}

export class PropertyMarker extends React.PureComponent<PropertyMarkerProps> {
    public render() {
        const {property, isSelected, isCentral} = this.props;
        return (
            <div className={cn.markerWrapper}>
                <span
                    className={classnames(cn.marker, {[cn.selected]: isSelected, [cn.central]: isCentral})}
                >
                    ${property.incomeGenerated}
                </span>
            </div>
        );
    }
}
