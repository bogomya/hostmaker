import React from "react";

import { City, Property } from "hostmaker/src/ducks/property/types";
import { PropertyCard } from "hostmaker/src/components/property/propertyCard/PropertyCard";
import { isPropertyCentral } from "hostmaker/src/ducks/selectors";

import * as cn from "./PropertyList.scss";

export type PropertyListStateProps = {
    properties: Property[];
    total: number;
    selectedProperty: Property | undefined;
    city: City;
};

export type PropertyListDispatchProps = {
    selectProperty: (property?: Property) => void;
};

export type PropertyListProps = PropertyListStateProps & PropertyListDispatchProps;

export class PropertyList extends React.PureComponent<PropertyListProps> {
    public render() {
        const { properties, selectedProperty, selectProperty, city, total} = this.props;
        return (
            <div>
                <div className={cn.total}>
                    {total > 0 && <span>{total} result(s) found</span>}
                    {total === 0 && (
                        <span className={cn.negative}>
                            <span className={cn.title}>No results.</span>
                            <span className={cn.description}>
                                To get more results, try adjusting your search by changing map
                            </span>
                        </span>
                    )}
                </div>
                <div className={cn.list}>
                    {properties.map((property) => (
                        <div className={cn.item} key={property.id}>
                            <PropertyCard
                                toggleSelection={selectProperty}
                                property={property}
                                isSelected={selectedProperty === property}
                                isCentral={isPropertyCentral(property, city)}
                            />
                        </div>),
                    )}
                </div>
            </div>
        );
    }
}
