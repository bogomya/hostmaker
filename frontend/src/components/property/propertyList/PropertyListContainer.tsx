import { connect } from "react-redux";
import { HostmakerState } from "hostmaker/src/ducks/state";
import {
    selectActiveProperty,
    selectCity,
    selectProperties,
    selectTotalProperties
} from "hostmaker/src/ducks/selectors";
import { selectProperty } from "hostmaker/src/ducks/property/property";
import {
    PropertyList,
    PropertyListDispatchProps,
    PropertyListStateProps,
} from "hostmaker/src/components/property/propertyList/PropertyList";

const mapStateToProps = (state: HostmakerState): PropertyListStateProps  => ({
    properties: selectProperties(state),
    selectedProperty: selectActiveProperty(state),
    city: selectCity(state),
    total: selectTotalProperties(state),
});

const dispatchProps: PropertyListDispatchProps = {
    selectProperty,
};

export const PropertyListContainer = connect<
    PropertyListStateProps,
    PropertyListDispatchProps,
    {},
    HostmakerState
>(mapStateToProps, dispatchProps)(PropertyList);
