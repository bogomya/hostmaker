import * as React from "react";
import { shallow } from "enzyme";

import { PropertyDetailsPage } from "hostmaker/src/layout/propertyDetailsPage/PropertyDetailsPage";
import { PropertyListContainer } from "hostmaker/src/components/property/propertyList/PropertyListContainer";
import { PropertyMapContainer } from "hostmaker/src/components/property/propertyMap/PropertyMapContainer";

describe("PropertyDetailsPage", () => {
    it("Should render list with properties", () => {
        const wrapper = shallow(<PropertyDetailsPage />);
        expect(wrapper.contains(<PropertyListContainer/>)).toBeTruthy();
    });
    it("Should render map with properties", () => {
        const wrapper = shallow(<PropertyDetailsPage />);
        expect(wrapper.contains(<PropertyMapContainer/>)).toBeTruthy();
    });
});

