export const page: string;
export const list: string;
export const mapWrapper: string;
export const map: string;
export const active: string;
export const mapIcon: string;
