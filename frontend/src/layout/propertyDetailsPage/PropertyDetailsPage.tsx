import * as React from "react";
import classnames from "classnames";

import { PropertyListContainer } from "hostmaker/src/components/property/propertyList/PropertyListContainer";
import { PropertyMapContainer } from "hostmaker/src/components/property/propertyMap/PropertyMapContainer";

import mapIcon from "hostmaker/src/layout/propertyDetailsPage/mapIcon.svg";
import * as cn from "hostmaker/src/layout/propertyDetailsPage/PropertyDetailsPage.scss";

export type PropertyDetailsPageProps = {};

export class PropertyDetailsPage extends React.PureComponent<PropertyDetailsPageProps> {
    public state = {
        mapIsVisible: false,
    };

    public toggleMobileMap = () => {
        this.setState({mapIsVisible: !this.state.mapIsVisible});
    }

    public render() {
        return (
            <div className={cn.page}>
                <div className={cn.list}>
                    <PropertyListContainer/>
                </div>
                <div className={cn.mapWrapper}>
                    <div className={classnames(cn.map, {[cn.active]: this.state.mapIsVisible})}>
                        <PropertyMapContainer />
                    </div>
               </div>
                <img className={cn.mapIcon} src={mapIcon} onClick={this.toggleMobileMap}/>
            </div>
        );
    }
}
