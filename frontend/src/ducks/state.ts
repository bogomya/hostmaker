import { combineReducers } from "redux";
import { PropertyState } from "hostmaker/src/ducks/property/types";
import { propertyReducer } from "hostmaker/src/ducks/property/property";

export type HostmakerState = {
    property: PropertyState,
};

export const rootReducer = combineReducers({
    property: propertyReducer,
});
