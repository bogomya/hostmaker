import { Property } from "hostmaker/src/ducks/property/types";

export function mockProperty(): Property {
    return {
        id: Math.random().toString(),
        owner: "owner",
        address: {
            line1: "line1",
            line4: "line4",
            postCode: "postCode",
            city: "city",
            country: "country",
        },
        coordinate: {
            lat: Math.random(),
            lng: Math.random(),
        },
        incomeGenerated: Math.random()
    }
}

export function mockCity() {
    return {
        title: "title",
        coordinate: {
            lat: Math.random(),
            lng: Math.random(),
        },
        radius: Math.random(),
    }
}
