export type PropertyAddress = {
    line1: string;
    line2?: string;
    line3?: string;
    line4: string;
    postCode: string;
    city: string;
    country: string;
};

export type Property = {
    id: string;
    owner: string;
    address: PropertyAddress;
    coordinate: Coordinate;
    incomeGenerated: number;
};

export type Coordinate = {
    lat: number;
    lng: number;
};

export type City = {
    title: string;
    coordinate: Coordinate;
    radius: number;
};

export type PropertyState = {
    properties: Property[];
    total: number;
    activeProperty: Property | undefined;
    city: City;
};
