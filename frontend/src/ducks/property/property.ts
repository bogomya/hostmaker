import { Action, Dispatch } from "redux";
import { Coordinate, Property, PropertyState } from "hostmaker/src/ducks/property/types";

interface PayloadAction<T, P> extends Action<T> {
    payload: P;
}

const SET_PROPERTIES = "property/SET_PROPERTIES";
const SELECT_PROPERTY = "property/SELECT_PROPERTY";

export type SetPropertiesAction = PayloadAction<typeof SET_PROPERTIES, {
    properties: Property[]
    total: number;
}>;

export type SelectPropertyAction = PayloadAction<typeof SELECT_PROPERTY, {
    property: Property | undefined,
}>;

export type PropertyActions = SetPropertiesAction | SelectPropertyAction;

function setPropertiesActionCreator(properties: Property[] = [], total: number = 0): SetPropertiesAction {
    return {
        type: SET_PROPERTIES,
        payload: {
            properties,
            total,
        },
    };
}

type SearchPropertyResult = {
    rows: Property[],
    total: number;
};

export function findPropertiesByCoordinates(northEast: Coordinate, southWest: Coordinate) {
    return async (dispatch: Dispatch<SetPropertiesAction>) => {
        try {
            const url = new URL(`${process.env.API_URL}/properties`);
            url.searchParams.append("ne", JSON.stringify(northEast));
            url.searchParams.append("sw", JSON.stringify(southWest));
            const response = await fetch(url.toString());
            const result: SearchPropertyResult = await response.json();
            dispatch(setPropertiesActionCreator(result.rows, result.total));
        } catch (e) {
            dispatch(setPropertiesActionCreator());
        }
    };
}

export function selectProperty(property?: Property) {
    return async (dispatch: Dispatch<SelectPropertyAction>) => {
        dispatch({
            type: SELECT_PROPERTY,
            payload: {
                property,
            },
        });
    };
}

const initialState: PropertyState = {
    properties: [],
    total: 0,
    activeProperty: undefined,
    city: {
        title: "London",
        coordinate: {
            lat: 51.5073835,
            lng: -0.1277801,
        },
        radius: 20000,
    },
};

export function propertyReducer(state: PropertyState = initialState, action: PropertyActions) {
    switch (action.type) {
        case SET_PROPERTIES:
            return {
                ...state,
                properties: action.payload.properties,
                total: action.payload.total,
            };
            break;
        case SELECT_PROPERTY:
            return {
                ...state,
                activeProperty: action.payload.property,
            };
            break;
        default:
            return state;
    }
}
