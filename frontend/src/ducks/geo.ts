import { Coordinate } from "hostmaker/src/ducks/property/types";

export function getDistance(from: Coordinate, to: Coordinate) {
    const { lat: lat1, lng: lng1} = from;
    const { lat: lat2, lng: lng2} = to;
    const R = 6371; // Radius of the earth in km
    const distanceLat = deg2rad(lat2 - lat1);
    const distanceLon = deg2rad(lng2 - lng1);

    const path =
        Math.sin(distanceLat / 2) * Math.sin(distanceLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(distanceLon / 2) * Math.sin(distanceLon / 2)
    ;
    return R * 2 * Math.atan2(Math.sqrt(path), Math.sqrt(1 - path)) * 1000;
}

function deg2rad(deg: number) {
    return deg * (Math.PI / 180);
}
