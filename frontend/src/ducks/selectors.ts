import { HostmakerState } from "hostmaker/src/ducks/state";
import { City, Property } from "hostmaker/src/ducks/property/types";
import { getDistance } from "hostmaker/src/ducks/geo";

export function selectProperties(state: HostmakerState) {
    return state.property.properties;
}

export function selectTotalProperties(state: HostmakerState) {
    return state.property.total;
}

export function selectActiveProperty(state: HostmakerState) {
    return state.property.activeProperty;
}

export function selectCity(state: HostmakerState) {
    return state.property.city;
}

export function isPropertyCentral(property: Property, city: City) {
    return getDistance(property.coordinate, city.coordinate) <= city.radius;
}
