const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");


const htmlPlugin = new HtmlWebpackPlugin({
    template: "./src/index.html",
    filename: "./index.html"
});

const cleanDistPlugin = new CleanWebpackPlugin(['dist']);

const cssExtractPlugin = new MiniCssExtractPlugin({
    filename: "[name].css",
    chunkFilename: "[id].css"
});

const ignoreStyleTypingsPlugin = new webpack.WatchIgnorePlugin([
    /scss\.d\.ts$/
]);

const hmrPlugin = new webpack.HotModuleReplacementPlugin();

const envPlugin = new webpack.EnvironmentPlugin(['GOOGLE_MAP_KEY', ['API_URL']]);

const uglifyJsPlugin = new UglifyJsPlugin({
    cache: true,
    parallel: true,
});
const optimizeCssPlugin = new OptimizeCSSAssetsPlugin();

module.exports = (env, argv) => {
    const isProduction =  argv.mode === "production";
    const config = {
        entry: './src/index.tsx',
        output: {
            filename: '[name].bundle.js',
            path: path.resolve(__dirname, 'dist')
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    use: 'ts-loader',
                    exclude: /node_modules/
                },
                {
                    test: /\.scss$/,
                    include: path.join(__dirname, 'src'),
                    use: [
                        isProduction ? MiniCssExtractPlugin.loader: "style-loader",
                        {
                            loader: 'typings-for-css-modules-loader',
                            options: {
                                modules: true,
                                localIdentName: "[local]--[hash:base64:6]",
                                namedExport: true,
                                camelCase: true,
                            }
                        },
                        "sass-loader",
                    ]
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: ['file-loader']
                },
                {
                    test: /\.(woff|woff2|eot|ttf|otf)$/,
                    use: ['file-loader']
                }
            ]
        },
        resolve: {
            alias: {
                hostmaker: path.resolve(__dirname, "./")
            },
            extensions: [ '.tsx', '.ts', '.js', '.scss']
        },
        plugins: [cleanDistPlugin, htmlPlugin, ignoreStyleTypingsPlugin, cssExtractPlugin, envPlugin],
        optimization: {
            splitChunks: {
                chunks: 'all'
            }
        },
    };

    if (isProduction) {
        config.optimization = {
            ...(config.optimization || {}),
            minimizer: [
                uglifyJsPlugin,
                optimizeCssPlugin
            ]
        }
    } else {
        config.devtool = false;
        config.plugins.push(new webpack.SourceMapDevToolPlugin({
            exclude: ['vendors~main.bundle.js'],
        }));

        config.devServer = {
            contentBase: false && path.resolve(__dirname, 'dist'),
            hot: true
        };
        config.plugins.push(hmrPlugin)
    }
    return config
};
