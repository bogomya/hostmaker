import { PropertyController } from "hostmaker/src/domain/property/PropertyController";
import { RouterProvider } from "hostmaker/src/infrastructure/Router";

export const propertyRouterProvider: RouterProvider = (router) => {
    router.get("/properties", PropertyController.search);
};
