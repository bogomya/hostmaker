import { Request, Response } from "express";
import {
    PropertyRepository,
    PropertySearchResult,
} from "hostmaker/src/domain/property/repositories/PropertyRepository";

export class PropertyController {
    public static async search(request: Request, response: Response) {
        let searchResult: PropertySearchResult = {rows: [], total: 0};
        const ne = JSON.parse(request.query.ne);
        const sw = JSON.parse(request.query.sw);

        if (sw && ne && sw.lat && sw.lng && ne.lat && ne.lng) {
            searchResult = await PropertyRepository.findByCoordinates(ne, sw);
        }

        response.json(searchResult);
    }
}
