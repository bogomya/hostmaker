import { Entity, Column, PrimaryGeneratedColumn, Index } from "typeorm";
import { Address } from "hostmaker/src/domain/property/entities/Address";
import { Coordinate } from "hostmaker/src/domain/property/entities/Coordinate";

@Entity()
export class Property {
    @PrimaryGeneratedColumn("uuid")
    private id: string;

    @Column()
    private owner: string;

    @Column({
        type: "point",
        transformer: {
            from: (value) => {
                const [lat, lng] = value.substring(6, value.length - 1).split(" ");
                return {lat, lng};
            },
            to: (value) => `POINT(${value.lat} ${value.lng})`,
        },
    })
    @Index({ spatial: true })
    private coordinate: Coordinate;

    @Column(() => Address)
    private address: Address;

    @Column("decimal", {precision: 13, scale: 2})
    private incomeGenerated: number;
}
