import { Column } from "typeorm";

export class Address {
    @Column()
    private line1: string;

    @Column({nullable: true})
    private line2: string;

    @Column({nullable: true})
    private line3: string;

    @Column()
    private line4: string;

    @Column()
    private postCode: string;

    @Column()
    private city: string;

    @Column()
    private country: string;
}
