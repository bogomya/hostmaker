import { getConnection} from "typeorm";
import { Property } from "hostmaker/src/domain/property/entities/Property";
import { Coordinate } from "hostmaker/src/domain/property/entities/Coordinate";

export type SearchResult<P> = {
    rows: P[],
    total: number;
};
export type PropertySearchResult = SearchResult<Property>;

export class PropertyRepository {
    public static async findByCoordinates(
        northEast: Coordinate,
        southWest: Coordinate,
    ): Promise<PropertySearchResult> {
        try {
            const [rows, total] = await getConnection()
                .createQueryBuilder()
                .select("p")
                .from(Property, "p")
                .where(
                    "MBRWithin(p.coordinate, Envelope(GeomFromText('LINESTRING(:neLat :neLng, :swLat :swLng)')))",
                )
                .orderBy(
                    // tslint:disable-next-line:max-line-length
                    "Distance(p.coordinate, ST_Centroid(Envelope(GeomFromText('LINESTRING(:neLat :neLng, :swLat :swLng)'))))",
                )
                .limit(50)
                .offset(0)
                .setParameters({
                    neLat: northEast.lat,
                    neLng: northEast.lng,
                    swLat: southWest.lat,
                    swLng: southWest.lng,
                })
                .getManyAndCount();

            return {
                rows,
                total,
            };
        } catch (e) {
            console.error(e);
            return {rows: [], total: 0};
        }
    }
}
