import "reflect-metadata";
import express from "express";
import bodyParser from "body-parser";
import { Express } from "express";
import cors from "cors";

import { createServer, Server } from "http";
import { createConnection, getConnectionOptions } from "typeorm";
import { AppLogger } from "hostmaker/src/infrastructure/Logger";
import { configureAppRouter } from "hostmaker/src/infrastructure/Router";
import { createHttpErrorHandlerMiddleware } from "hostmaker/src/infrastructure/HttpErrorHandler";
import { propertyRouterProvider } from "hostmaker/src/domain/property/routing";

const logger = new AppLogger();
const app: Express = express();
const server: Server = createServer(app);

app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(cors());
app.use(configureAppRouter(express.Router(), [propertyRouterProvider]));
app.use(createHttpErrorHandlerMiddleware(logger));

server.listen(process.env.APP_PORT || 8080, () => {
    logger.log("Server is started!");
});

server.on("error", (error: Error) => {
    logger.error(error);
});

(async function connectToDatabase() {
    try {
        const connectionOptions = await getConnectionOptions();
        const isProduction = process.env.NODE_ENV === "production";
        await createConnection({
            ...connectionOptions,
            logging: isProduction ? ["error"] : "all",
            entities: isProduction ? ["dist/domain/**/entities/*.js"] : ["src/domain/**/entities/*.ts"],
            migrations: isProduction ? ["dist/domain/**/entities/*.js"] : ["src/migrations/**/*ts"],
        });
    } catch (e) {
        logger.error(e.toString());
        server.close();
    }
})();
