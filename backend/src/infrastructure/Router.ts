// @ts-ignore
import { Router } from "express";

export type RouterProvider = (router: Router) => void;

export function configureAppRouter(router: Router, routeProviders: RouterProvider[]) {
    routeProviders.forEach((provider: RouterProvider) => {
        provider(router);
    });
    return router;
}
