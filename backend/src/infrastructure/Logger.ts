import * as winston from "winston";
import { Logger } from "winston";

export interface AppLoggerInterface {
    log(message: any): void;
    error(message: any, meta?: any): void;
}

export class AppLogger implements AppLoggerInterface {
    private logger: Logger;

    public constructor() {
        this.logger = winston.createLogger({
            level: "info",
            format: winston.format.json(),
            transports: [
                new winston.transports.File({ filename: "./logs/error.log", level: "error" }),
                new winston.transports.File({ filename: "./logs/combined.log" }),
            ],
        });

        if (process.env.NODE_ENV !== "production") {
            this.logger.add(new winston.transports.Console({
                format: winston.format.colorize(),
                level: "silly",
            }));
        }
    }

    public log(message: any): void {
        this.logger.info(message);
    }

    public error(message: any, meta?: any): void {
        this.logger.error(message, meta);
    }
}
