import { NextFunction, Request, Response } from "express";
import { AppLoggerInterface } from "hostmaker/src/infrastructure/Logger";

export function createHttpErrorHandlerMiddleware(logger: AppLoggerInterface) {
    return function errorHandlerMiddleware(err: Error, req: Request, res: Response, next: NextFunction) {
        if (!err) {
            next();
        }
        logger.error(err.message, err.stack);

        if (err instanceof SyntaxError && err && (err as any).type === "entity.parse.failed") {
            return res.status(400).json({
                error: "Bad Request",
            });
        }

        return res.status(500).json({
            error: "Internal Server Error, please try later",
        });
    };
}
