import {MigrationInterface, QueryRunner} from "typeorm";

// tslint:disable-next-line
export class initialSetup1539373084229 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        // tslint:disable-next-line:max-line-length
        await queryRunner.query("CREATE TABLE `property` (`id` varchar(255) NOT NULL, `owner` varchar(255) NOT NULL, `coordinate` point NOT NULL, `incomeGenerated` decimal(13,2) NOT NULL, `addressLine1` varchar(255) NOT NULL, `addressLine2` varchar(255) NULL, `addressLine3` varchar(255) NULL, `addressLine4` varchar(255) NOT NULL, `addressPostcode` varchar(255) NOT NULL, `addressCity` varchar(255) NOT NULL, `addressCountry` varchar(255) NOT NULL, SPATIAL INDEX `IDX_617b7f388d6e3f9cc62af327b8` (`coordinate`), PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("DROP INDEX `IDX_617b7f388d6e3f9cc62af327b8` ON `property`");
        await queryRunner.query("DROP TABLE `property`");
    }

}
