import "reflect-metadata";
import fixtures from "./fixtures.json";
import { createConnection, getConnectionOptions } from "typeorm";
import { Property } from "hostmaker/src/domain/property/entities/Property";

(async function fixtureLoader() {
    try {
        const connectionOptions = await getConnectionOptions();
        const connection = await createConnection({...connectionOptions, logging: "all"});
        const repository = await connection.getRepository(Property);
        await repository.clear();
        for (const fixture of fixtures) {
            await repository
                .createQueryBuilder()
                .insert()
                .values(fixture)
                .execute();
        }
        connection.close();
    } catch (e) {
        console.log(e);
    }
})();
